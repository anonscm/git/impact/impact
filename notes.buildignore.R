## This tool requires the flowCore library
if (!require("flowCore")) {
  cat("flowCore library not present, trying to install flowCore\n")
  source("http://bioconductor.org/biocLite.R")
  biocLite("flowCore")
  if (require("flowCore")) {
    cat("flowCore library installed successfully\n")
  } else {
    stop("Failed to install the flowCore library, please try installing flowCore manually and try again.", call. = FALSE)
  }
}


# http://www.ats.ucla.edu/stat/r/examples/asa/
hmohiv<-read.table("http://www.ats.ucla.edu/stat/r/examples/asa/hmohiv.csv", sep=",", header = TRUE)
hmohiv$ent.date = as.Date(gsub("(\\d+)/(\\d+)/(\\d+)\\s+", "\\3-\\1-\\2", hmohiv$entdate))
hmohiv$end.date = as.Date(gsub("(\\d+)/(\\d+)/(\\d+)\\s+", "\\3-\\1-\\2", hmohiv$enddate))
hmohiv$OS = hmohiv$censor
hmohiv$OS.time = round(as.numeric(hmohiv$end.date - hmohiv$ent.date) / 30.5, 3)
hmohiv$RFS.time = pmin(40, hmohiv$OS.time)
hmohiv$RFS = ifelse(hmohiv$RFS.time == hmohiv$OS.time, hmohiv$OS, 0)
#write.csv(hmohiv, file = "ucla-idre-hmohiv.csv")
write.table(hmohiv, file = "ucla-idre-hmohiv.txt", sep = "\t")



# https://gist.github.com/trestletech/8608815
# http://stackoverflow.com/questions/21282228/update-plot-within-observer-loop-in-shiny-application

runApp(shinyApp(
  ui = shinyUI(pageWithSidebar(

  # Application title
  headerPanel("New Application"),

  sidebarPanel(
    "Progress: ",
    textOutput("counter"),
    hr(),
    "Elapsed Time (seconds):",
    textOutput("elapsed")
  ),

  mainPanel(
    textOutput("x")
  )
)),

server = shinyServer(function(input, output, session) {
  # The number of iterations to perform
  maxIter <- 50

  # Track the start and elapsed time
  startTime <- Sys.time()
  output$elapsed <- renderText({
    vals$x
    round(Sys.time() - startTime)
  })

  # Create a reactiveValues object where we can track some extra elements
  # reactively.
  vals <- reactiveValues(x = 0, counter = 0)

  # Update the percentage complete
  output$counter <- renderText({
    paste0(round(vals$counter/maxIter * 100, 1), "%")
  })

  # Show the value of x
  output$x <- renderText({
    round(vals$x,2)
  })

  # Do the actual computation here.
  observe({
    isolate({
      # This is where we do the expensive computing
      sum <- 0
      for (i in 1:100000){
        sum <- sum + rnorm(1)
      }
      vals$x <- vals$x + sum

      # Increment the counter
      vals$counter <- vals$counter + 1
    })

    # If we're not done yet, then schedule this block to execute again ASAP.
    # Note that we can be interrupted by other reactive updates to, for
    # instance, update a text output.
    if (isolate(vals$counter) < maxIter){
      invalidateLater(0, session)
    }
  })

})
))


runApp(shinyApp(
  ui = shinyUI(pageWithSidebar(
    headerPanel("New Application"),
    sidebarPanel("Progress: ",textOutput("counter"),
                 textInput("maxIter", "Max iter", "0"),
                 actionButton("go", "Go!")),
    mainPanel("Hello!")
  )),
  server = shinyServer(function(input, output, session) {
    # Create a reactiveValues object where we can track some extra elements
    # reactively.
    vals <- reactiveValues(counter = NULL, maxIter = NULL)
    # Update the percentage complete
    output$counter <- renderText({
      if (is.null(vals$maxIter)) return()
      sprintf("%.0f%% %d %d", vals$counter/vals$maxIter * 100, vals$counter, vals$maxIter)
    })
    # Do the actual computation here.
    observeEvent(input$go, {
        vals$maxIter <- as.numeric(input$maxIter)
        vals$counter <- 0
    })
    observe({
      if (is.null(vals$maxIter)) return()
      isolate({
        # This is where we do the expensive computing
        Sys.sleep(0.4)
        # Increment the counter
        vals$counter <- vals$counter + 1
      })
      # If we're not done yet, then schedule this block to execute again ASAP.
      # Note that we can be interrupted by other reactive updates to, for
      # instance, update a text output.
      if (isolate(vals$counter) < vals$maxIter){
        invalidateLater(1, session)
      }
    })
  })
))
